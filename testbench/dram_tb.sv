module dram_tb();

// testbench counters
parameter VALS = 4;
integer cycle;
reg [31:0] val1;
reg [31:0] val8;
reg [31:0] rdrq;

// DUT signals
reg memclk;
reg rst_n;
reg [15:0] address1;
reg [15:0] address8;
wire [31:0] data1;
wire [31:0] data8;
reg [31:0] write_data1;
reg [31:0] write_data8;
reg write1;
reg write8;
reg read1;
reg read8;
wire valid1;
wire valid8;
wire ready1;
wire ready8;

// has to be this way because .data is an inout
assign data1 = write1 ? write_data1 : 32'hZZZZZZZZ;
assign data8 = write8 ? write_data8 : 32'hZZZZZZZZ;

// 1-cycle dram
dram #(
    .DRAM_LATENCY(1),
    .DRAM_ADDR_BITS(12),
    .DRAM_DATA_BITS(32),
    .DRAM_TEXT_START(0)
    ) dram1 (
    .memclk(memclk),
    .rst_n(rst_n),
    .address(address1),
    .data(data1),
    .write(write1),
    .read(read1),
    .valid(valid1),
    .ready(ready1)
);

// 8-cycle dram
dram #(
    .DRAM_LATENCY(8),
    .DRAM_ADDR_BITS(12),
    .DRAM_DATA_BITS(32),
    .DRAM_TEXT_START(0)
    ) dram8 (
    .memclk(memclk),
    .rst_n(rst_n),
    .address(address8),
    .data(data8),
    .write(write8),
    .read(read8),
    .valid(valid8),
    .ready(ready8)
);


always begin
    #5 memclk = ~memclk;
end

always @(posedge memclk) begin
    cycle = cycle + 1;
    if (cycle > 10000) begin
        $display("Simulation timed out");
        $stop;
    end
end

initial begin
    cycle = 0;
    val1 = 0;
    val8 = 0;
    rdrq = 0;
    memclk = 0;
    rst_n = 0;
    address1 = 0;
    address8 = 0;
    write_data1 = 0;
    write_data8 = 0;
    write1 = 0;
    write8 = 0;
    read1 = 0;
    read8 = 0;

    @(negedge memclk);
    rst_n = 1;
    @(negedge memclk);


    // write some data to both drams
    fork
        // writer1
        begin
            while (val1 < VALS) begin
                address1 = 0;
                write_data1 = 0;
                write1 = 1'b0;
                if (ready1) begin
                    address1 = val1 * 4;
                    write_data1 = val1;
                    write1 = 1'b1;
                    val1 = val1 + 1;
                    $display("Writing %x to address %x in dram1 at cycle %d", write_data1, address1, cycle);
                end
                @(negedge memclk);
            end
            address1 = 0;
            write_data1 = 0;
            write1 = 1'b0;
            val1 = 0;
        end 
        
        // writer8
        begin
            while (val8 < VALS) begin
                address8 = 0;
                write_data8 = 0;
                write8 = 1'b0;
                if (ready8) begin
                    address8 = val8 * 4;
                    write_data8 = val8;
                    write8 = 1'b1;
                    val8 = val8 + 1;
                    $display("Writing %x to address %x in dram8 at cycle %d", write_data1, address1, cycle);
                end
                @(negedge memclk);
            end
            address8 = 0;
            write_data8 = 0;
            write8 = 1'b0;
            val8 = 0;
        end 
    join

    // read everything from the drams
    fork
        // send a bunch of read requests to both drams
        begin 
            while (rdrq < VALS) begin
                $display("Sending read request %x at cycle %d", rdrq, cycle);
                address1 = rdrq * 4;
                address8 = rdrq * 4;
                read1 = 1;
                read8 = 1;
                rdrq = rdrq + 1;
                @(negedge memclk);
            end
            address1 = 0;
            address8 = 0;
            read1 = 0;
            read8 = 0;
            rdrq = 0;
        end

        // wait for the requests to be fulfilled for dram1
        begin
            while (val1 < VALS) begin
                if (valid1) begin
                    $display("Read %x from dram1 at cycle %d", data1, cycle);
                    val1 = val1 + 1;
                end
                @(negedge memclk);
            end
        end

        // wait for the requests to be fulfilled for dram8
        begin
            while (val8 < VALS) begin
                if (valid8) begin
                    $display("Read %x from dram8 at cycle %d", data8, cycle);
                    val8 = val8 + 1;
                end
                @(negedge memclk);
            end
        end
    join

    $stop;

end



endmodule
