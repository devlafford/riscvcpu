module physical_register_file_tb();

    parameter READ_PORT_PAIRS = 2;
    parameter WRITE_PORTS = 2;
    parameter PHYS_REG_COUNT = 128;

    reg clk, rst_n;
    reg wren[WRITE_PORTS];
    reg [$clog2(PHYS_REG_COUNT):0] wr_addr[WRITE_PORTS];
    reg [63:0] wr_data[WRITE_PORTS];
    reg rd1en[READ_PORT_PAIRS];
    reg rd2en[READ_PORT_PAIRS];
    reg [$clog2(PHYS_REG_COUNT):0] src1_addr[READ_PORT_PAIRS];
    reg [$clog2(PHYS_REG_COUNT):0] src2_addr[READ_PORT_PAIRS];
    wire [63:0] src1_data[READ_PORT_PAIRS];
    wire [63:0] src2_data[READ_PORT_PAIRS];

    physical_register_file#(
        .READ_PORT_PAIRS(READ_PORT_PAIRS),
        .WRITE_PORTS(WRITE_PORTS),
        .PHYS_REG_COUNT(PHYS_REG_COUNT)
    ) rf1 (
        .clk(clk),
        .rst_n(rst_n),
        .wren(wren),
        .wr_addr(wr_addr),
        .wr_data(wr_data),
        .rd1en(rd1en),
        .rd2en(rd2en),
        .src1_addr(src1_addr),
        .src2_addr(src2_addr),
        .src1_data(src1_data),
        .src2_data(src2_data)
    );

always #5 clk <= ~clk;

initial begin
    clk = 0;
    rst_n = 0;
    wren[0] = 0;
    wren[1] = 0;
    wr_addr[0] = 0;
    wr_addr[1] = 0;
    wr_data[0] = 0;
    wr_data[1] = 0;
    rd1en[0] = 0;
    rd1en[1] = 0;
    rd2en[0] = 0;
    rd2en[1] = 0;
    src1_addr[0] = 0;
    src1_addr[1] = 0;
    src2_addr[0] = 0;
    src2_addr[1] = 0;

    @(negedge clk)
    rst_n = 1;
    
    // write a few values to the regfile simultaneously
    @(negedge clk)
    wren[0] = 1;
    wren[1] = 1;
    wr_addr[0] = 1;
    wr_addr[1] = 2;
    wr_data[0] = 100;
    wr_data[1] = 200;

    @(negedge clk)
    wren[0] = 1;
    wren[1] = 1;
    wr_addr[0] = 3;
    wr_addr[1] = 4;
    wr_data[0] = 300;
    wr_data[1] = 400;

    @(negedge clk)
    wren[0] = 1;
    wren[1] = 1;
    wr_addr[0] = 5;
    wr_addr[1] = 6;
    wr_data[0] = 500;
    wr_data[1] = 600;

    @(negedge clk)
    wren[0] = 1;
    wren[1] = 1;
    wr_addr[0] = 7;
    wr_addr[1] = 8;
    wr_data[0] = 700;
    wr_data[1] = 800;

    @(negedge clk)
    wren[0] = 0;
    wren[1] = 0;
    wr_addr[0] = 0;
    wr_addr[1] = 0;
    wr_data[0] = 000;
    wr_data[1] = 000;

    // read some values simultaneously
    $monitor("src1_data[0]:%d\nsrc2_data[0]:%d\nsrc1_data[1]:%d\nsrc2_data[1]:%d\n", src1_data[0], src2_data[0], src1_data[1], src2_data[1]);

    @(negedge clk)
    rd1en[0] = 1;
    rd2en[0] = 1;
    rd1en[1] = 1;
    rd2en[1] = 1;
    src1_addr[0] = 1;
    src2_addr[0] = 2;
    src1_addr[1] = 3;
    src2_addr[1] = 4;

    @(negedge clk)
    rd1en[0] = 1;
    rd2en[0] = 1;
    rd1en[1] = 1;
    rd2en[1] = 1;
    src1_addr[0] = 1;
    src2_addr[0] = 2;
    src1_addr[1] = 3;
    src2_addr[1] = 4;

    @(negedge clk)
    rd1en[0] = 1;
    rd2en[0] = 1;
    rd1en[1] = 1;
    rd2en[1] = 1;
    src1_addr[0] = 5;
    src2_addr[0] = 6;
    src1_addr[1] = 7;
    src2_addr[1] = 8;

    @(negedge clk)
    rd1en[0] = 0;
    rd2en[0] = 0;
    rd1en[1] = 0;
    rd2en[1] = 0;
    src1_addr[0] = 0;
    src2_addr[0] = 0;
    src1_addr[1] = 0;
    src2_addr[1] = 0;

    // try writing to r0 and reading from it
    @(negedge clk)
    wren[0] = 1;
    wr_addr[0] = 0;
    wr_data[0] = 111;

    @(negedge clk)
    rd1en[0] = 1;
    rd2en[0] = 1;
    src1_addr[0] = 0;
    src2_addr[0] = 0;

    @(negedge clk)
    rd1en[0] = 0;
    rd2en[0] = 0;
    src1_addr[0] = 0;
    src2_addr[0] = 0;

    $stop;

end


endmodule
