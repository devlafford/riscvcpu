module dram
    #(
    parameter DRAM_LATENCY = 1,
    parameter DRAM_ADDR_BITS = 32,
    parameter DRAM_DATA_BITS = 32,
    parameter DRAM_TEXT_START = 32'h00400000
    )(
    input memclk,
    input rst_n,
    input [DRAM_ADDR_BITS-1:0] address,
    inout [DRAM_DATA_BITS-1:0] data,
    input write,
    input read,
    output valid,
    output ready // ready is specifically for writes - you can send read rqs at any time
);

reg [7:0] memory[2**DRAM_ADDR_BITS];
reg [31:0] data_fifo[DRAM_LATENCY];
reg valid_fifo[DRAM_LATENCY];

// initial contents of the memory
initial begin
        static integer i = 0;
        static integer file = $fopen("../code/init.mem", "r");
        reg [31:0] data;
        $display("Attempting to init memory...");
        while ($fscanf(file, "%h\n", data) == 1) begin
            $display("Read %x from file at address %d", data, i);
            memory[i+0] <= data[07:00];
            memory[i+1] <= data[15:08];
            memory[i+2] <= data[23:16];
            memory[i+3] <= data[31:24];
            i = i + 4;
        end
end

// writes happen instantly, this is irrelevant to the dram model
// writes are little endian
always @(posedge memclk) begin
    if (write) begin
        memory[address+0] <= data[07:00];
        memory[address+1] <= data[15:08];
        memory[address+2] <= data[23:16];
        memory[address+3] <= data[31:24];
    end
end

// insert data into the head of the delayed request fifo
always @(posedge memclk, negedge rst_n) begin
    if (~rst_n) begin // start with nothing in the FIFO
        data_fifo[0] <= 32'h00000000;
        valid_fifo[0] <= 1'b0;
    end
    else if (read) begin // reads are delayed by DRAM_LATENCY
        data_fifo[0] <= {memory[address+3], memory[address+2], memory[address+1], memory[address+0]};
        valid_fifo[0] <= 1'b1;
    end
    else begin // nothing to put in fifo
        data_fifo[0] <= 32'h00000000;
        valid_fifo[0] <= 1'b0;
    end
end

// shift intermediate values along the FIFO
always @(posedge memclk, negedge rst_n) begin
    for (integer i = 1; i < DRAM_LATENCY; i = i + 1) begin
        if (~rst_n) begin
            data_fifo[i] <= 32'h00000000;
            valid_fifo[i] <= 1'b0;
        end
        else begin
            data_fifo[i] <= data_fifo[i-1];
            valid_fifo[i] <= valid_fifo[i-1];
        end
    end
end

// output the tail of the FIFOs
assign data = valid ? data_fifo[DRAM_LATENCY-1] : 32'hZZZZZZZZ;
assign valid = valid_fifo[DRAM_LATENCY-1];
assign ready = ~valid;

endmodule
