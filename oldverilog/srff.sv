module srff(
    input clk,
    input rst_n,
    input set,
    input reset,
    output reg q
);

always @(posedge clk, negedge rst_n) begin
    if (~rst_n) q <= 0;
    else if (set) q <= 1;
    else if (reset) q <= 0;
    else q <= q;
end

endmodule
