module single_cycle_cpu(
    input clk,
    input rst_n,
    output [31:0] dram_address,
    inout [31:0] dram_data,
    output dram_write,
    output dram_read,
    input dram_ready,
    input dram_valid
);


endmodule
