`ifndef _parameters_
`define _parameters_


// fetch
parameter FETCH_BUFFER_SIZE_H = 16;
parameter FETCH_WIDTH_H = 4;

// decode
parameter INSTRUCTION_SIZE_H = 32;
parameter DECODE_WIDTH_H = 4;

// memory
parameter DRAM_LATENCY_H = 20;
parameter DRAM_ADDR_BITS_H = 32;
parameter DRAM_DATA_BITS_H = 64;
parameter DRAM_TEXT_START_H = 32'h00400000;

//TODO: sort
parameter ROB_ENTRIES_H = 64;
parameter READ_PORT_PAIRS_H = 2;
parameter WRITE_PORTS_H = 2;
parameter PHYS_REG_COUNT_H = 128;

`endif
