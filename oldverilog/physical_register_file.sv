`include "parameters.svh"
module physical_register_file#(
    parameter READ_PORT_PAIRS = READ_PORT_PAIRS_H, // default 1 -> 2 read ports
    parameter WRITE_PORTS = WRITE_PORTS_H,
    parameter PHYS_REG_COUNT = PHYS_REG_COUNT_H
    )(
    input clk,
    input rst_n,

    // write ports
    input wren[WRITE_PORTS],
    input [$clog2(PHYS_REG_COUNT):0] wr_addr[WRITE_PORTS],
    input [63:0] wr_data[WRITE_PORTS],

    // read ports
    input rd1en[READ_PORT_PAIRS],
    input rd2en[READ_PORT_PAIRS],
    input [$clog2(PHYS_REG_COUNT):0] src1_addr[READ_PORT_PAIRS],
    input [$clog2(PHYS_REG_COUNT):0] src2_addr[READ_PORT_PAIRS],
    output [63:0] src1_data[READ_PORT_PAIRS],
    output [63:0] src2_data[READ_PORT_PAIRS]
);

    // regs
    reg [63:0] regs[PHYS_REG_COUNT];
    integer j;
    always @(posedge clk, negedge rst_n) begin
        if (~rst_n) begin
            for (integer i = 0; i < PHYS_REG_COUNT; i = i + 1) begin
                regs[i] <= 0;
            end
        end
        else begin // write mechanism; don't allow writes to preg0
            for (j = 0; j < WRITE_PORTS; j = j + 1) begin
                if (wren[j] && wr_addr[j] != 0) regs[wr_addr[j]] <= wr_data[j];
            end
        end
    end

    // outputs
    genvar k;
    generate
    for (k = 0; k < READ_PORT_PAIRS; k = k + 1) begin
        assign src1_data[k] = rd1en[k] ? regs[src1_addr[k]] : 0;
        assign src2_data[k] = rd2en[k] ? regs[src2_addr[k]] : 0;
    end
    endgenerate



endmodule
