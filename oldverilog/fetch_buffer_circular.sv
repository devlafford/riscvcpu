`include "parameters.svh"
module fetch_buffer #(
    parameter BUFFER_SIZE = FETCH_BUFFER_SIZE_H;
    parameter FETCH_WIDTH = FETCH_WIDTH_H;
    parameter DECODE_WIDTH = DECODE_WIDTH_H;
    parameter INSTR_SIZE = INSTRUCTION_SIZE_H;
    )(
    input clk, rst_n,
    input words_ready,
    input [INSTR_SIZE-1:0] words[FETCH_WIDTH],
    output fetched,
    output [INSTR_SIZE-1:0] instrs_to_decode[DECODE_WIDTH],
    output [$clog2(DECODE_WIDTH)-1:0] num_instrs_out,
    output [INSTR_SIZE-1:0] branch_instr,
    output predict,
    input clear_buffer
    );

    // fetch buffer memory
    reg [INSTR_SIZE-1:0] fetch_buffer[BUFFER_SIZE];
    reg [$clog2(BUFFER_SIZE)-1:0] head;
    reg [$clog2(BUFFER_SIZE)-1:0] tail;

    // determine if there is space in the buffer
    // check head -> head + fetch_size to see if any are ==tail
    // if any are tail, there is no space for a fetch
    // this probably synths to FETCH_WIDTH adders based on head into comparators with tail
    wire space_available;
    wire [$clog2(BUFFER_SIZE)-1:0] trunc;
    wire is_tail[FETCH_WIDTH];

    always_comb begin
        for (integer i = 0; i < FETCH_WIDTH; i = i + 1) begin
            trunc = head + i;
            if (trunc == tail) is_tail[i] = 1;
            else is_tail[i] = 0;
        end
    end

    assign space_available = ~|is_tail; 
    
    // do the fetching
    // some decoders here
    always_ff @(posedge clk, negedge rst_n) begin
        if (~rst_n) begin // init to zero
            for (integer i = 0; i < BUFFER_SIZE; i = i + 1) begin
                fetch_buffer[i] <= 0;
            end
        end
        else if (words_ready && space_available) begin // fetch x number of words into the buffer
            for (integer i = 0; i < BUFFER_SIZE; i = i + 1) begin
                fetch_buffer[head + i] = words[i];
        end
        else begin
            // do nothing
            $display("Fetch buffer full.");
        end
    end

    // advance head pointer and signal that we accepted the fetch
    assign fetched = words_ready && space_available;
    always_ff @(posedge clk, negedge rst_n) begin
        if (~rst_n) head <= 0;
        else if (fetched) head <= head + 4;
        else head <= head;
    end

    // decide which instructions to send out
    // send everything up to the first branch (or max bw)
    // send first branch to predictor
    // if it predicts not taken, clear the buffer (head & tail <= 0)
    wire is_branch[BUFFER_SIZE];
    wire send_instruction[DECODE_WIDTH];
    wire buffer_empty;
    assign buffer_empty = (head == tail);

    always_comb begin
        for (integer i = 0; i < BUFFER_SIZE; i = i + 1) begin
            is_branch = (fetch_buffer[i][6:5] == 2'b11) && (
                        fetch_buffer[i][4:2] == 3'b000 || // BRANCH
                        fetch_buffer[i][4:2] == 3'b001 || // JALR
                        fetch_buffer[i][4:2] == 3'b011 || // JAL
                        );
        end
    end

    



);



endmodule
