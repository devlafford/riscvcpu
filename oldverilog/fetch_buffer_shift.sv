`include "parameters.svh"
module fetch_buffer #(
    parameter BUFFER_SIZE = FETCH_BUFFER_SIZE_H;
    parameter FETCH_WIDTH = FETCH_WIDTH_H;
    parameter DECODE_WIDTH = DECODE_WIDTH_H
    parameter INSTR_SIZE = INSTRUCTION_SIZE_H;
    )(
    input clk, rst_n,
    input words_ready,
    input [INSTR_SIZE-1:0] words[FETCH_WIDTH],
    output fetched,
    output [INSTR_SIZE-1:0] instrs_to_decode[DECODE_WIDTH],
    output [$clog2(DECODE_WIDTH)-1:0] num_instrs_out,
    output reg [INSTR_SIZE-1:0] branch_instr,
    output reg predict,
    input clear_buffer
    );

    // fetch buffer memory
    reg [INSTR_SIZE-1:0] fetch_buffer[BUFFER_SIZE];
    reg [$clog2(BUFFER_SIZE)-1:0] instr_count;
    reg [$clog2(BUFFER_SIZE)-1:0] instr_count_next;
    wire space_available;

    // determine if there is space in the buffer
    // if size - instr_count > fetch width, there is no room
    assign space_available = (BUFFER_SIZE - instr_count) > FETCH_WIDTH;
    assign fetched = words_ready && space_available;

    // do the fetching
    // TODO: this will need to be a more intelligent generate statement
    always_ff @(posedge clk, negedge rst_n) begin
        if (~rst_n | clear_buffer) begin // init to zero or clear buffer
            for (integer i = 0; i < BUFFER_SIZE; i = i + 1) begin
                fetch_buffer[i] <= 0;
            end
        end
        else if (words_ready && space_available) begin // fetch into the buffer
            for (integer i = 0; i < FETCH_WIDTH; i = i + 1) begin
                fetch_buffer[instr_count + i] <= words[i];
            end
        end
        else begin
            // do nothing
            $display("Fetch buffer full.");
        end
    end

    // instr_count
    always_ff @(posedge clk, negedge rst_n) begin
        if (~rst_n) instr_count <= 0;
        else instr_count <= instr_count_next;
    end

    // should we advance the fetch pc or no

    // decide which instructions to send out
    wire is_branch[BUFFER_SIZE];
    wire send_instruction[DECODE_WIDTH];
    wire buffer_empty;
    assign buffer_empty = (head == tail);

    always_comb begin
        // comparators on first DECODE_WIDTH instructions to see if any are branch
        for (integer i = 0; i < DECODE_WIDTH; i = i + 1) begin
            is_branch[i] = (fetch_buffer[i][6:5] == 2'b11) && (
                        fetch_buffer[i][4:2] == 3'b000 || // BRANCH
                        fetch_buffer[i][4:2] == 3'b001 || // JALR
                        fetch_buffer[i][4:2] == 3'b011 || // JAL
                        );
        end
        predict = |is_branch;

        // priority encoder basically for sending first branch instr found or zero
        branch_instr = 0; // zero by default
        for (integer i = DECODE_WIDTH-1; i <= 0; i = i - 1) begin
            if (is_branch[i]) branch_instr = fetch_buffer[i];
        end

        // count how many instructions are not covered by a branch, up to DECODE_WIDTH
        num_instrs_out = 0;
        for (integer i = 0; i < DECODE_WIDTH; i = i + 1) begin
            if (~|is_branch[i:0]) num_instrs_out = num_instrs_out + 1;
        end
        if (num_instrs_out > instr_count) num_instrs_out = instr_count; // don't count blank instrs

        // equivalent to an assign statement, but works with parameters
        for (integer i = 0; i < DECODE_WIDTH; i = i + 1) begin
            instrs_to_decode[i] = fetch_buffer[i];
        end

        // figure out instr_count_next
        instr_count_next = 0;
        if (fetched) instr_count_next = instr_count + 4;
        instr_count_next = instr_count_next - num_instrs_out;
    end



);



endmodule
