`include "parameters.svh"
module ROB#(
    parameter ROB_ENTRIES = ROB_ENTRIES_H,
    parameter PHYS_REG_COUNT = PHYS_REG_COUNT_H
)(

input set_preg_used[PHYS_REG_COUNT];
input set_preg_open[PHYS_REG_COUNT];

);

// create the ROB entries
generate
for (genvar i = 0; i < ROB_ENTRIES; i = i + 1) begin
    ROBEntry entry (
    .clk(clk),
    .rst_n(rst_n),
    .head_set(),
    .head_reset(),
    .tail_set(),
    .tail_reset(),
    .dispatched_set(),
    .dispatched_reset(),
    .executed_set(),
    .executed_reset(),
    .allocated_set(),
    .allocated_reset(),

    .dstreg_in(),
    .dstreg_old_in(), // this preg is return upon instr complete
    .src1reg_in(),
    .src2reg_in(),

    .head(),
    .tail(),
    .dispatched(),
    .executed(),
    .allocated(),
    .dstreg(),
    .dstreg_old(),
    .src1reg(),
    .src2reg()
end

// bunch of srffs to keep track of which registers are in-flight to avoid RAW hazards

reg preg_in_use[PHYS_REG_COUNT];
always_ff @(posedge clk, negedge rst_n) begin
    for (genvar i = 0; i < PHYS_REG_COUNT; i= i + 1) begin
        if (~rst_n) reg_in_use[i] <= 0;
        else 

    end

end


endgenerate


