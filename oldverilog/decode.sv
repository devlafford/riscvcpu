`include "parameters.svh"
module decode#(
    parameter DECODE_WIDTH = DECODE_WIDTH_H
)(
    input [31:0] instr[DECODE_WIDTH];
    // output some signals, don't know what they are yet

);

wire funct7[DECODE_WIDTH];
wire rs2[DECODE_WIDTH];
wire rs1[DECODE_WIDTH];
wire funct3[DECODE_WIDTH];
wire rd[DECODE_WIDTH];
wire opcode[DECODE_WIDTH];
wire [63:0] imm_rtype[DECODE_WIDTH];
wire [63:0] imm_itype[DECODE_WIDTH];
wire [63:0] imm_stype[DECODE_WIDTH];
wire [63:0] imm_btype[DECODE_WIDTH];
wire [63:0] imm_utype[DECODE_WIDTH];
wire [63:0] imm_jtype[DECODE_WIDTH];

genvar i;
generate
for (i = 0; i < DECODE_WIDTH; i = i + 1) begin
    // r-type
    assign funct7[i] =     instr[31:25];
    assign rs2[i] =        instr[24:20];
    assign rs1[i] =        instr[19:15];
    assign funct3[i] =     instr[14:12];
    assign rd[i] =         instr[11:7];
    assign opcode[i] =     instr[6:0];

    // i-type
    assign imm_itype[i] = {{21}instr[31], instr[30:25], instr[24:21], instr[20]};

    // s-type
    assign imm_stype[i] = {{21}instr[31], instr[30:25], instr[11:8], instr[7]};

    // b-type
    assign imm_btype[i] = {{20}instr[31], instr[7], instr[30:25], instr[11:8] 1'b0};

    // u-type
    assign imm_jtype[i] = {instr[31], instr[30:20], instr[19:12], 12'b0};

    // j-type
    assign imm_jtype[i] = {{12}instr[31], instr[19:12], instr[20], instr[30:25], instr[24:21] 1'b0};
end
endgenerate

// put the branch predictor(s?) here somewhere

// page 103 of RISCV spec
always_comb begin
    case(instr[6:2])

        // LOAD
        5'b00000: begin
        end
        
        // OP-IMM
        5'b00100: begin
        end
        
        // AUIPC
        5'b00101: begin
        end
        
        // OP-IMM-32
        5'b00110: begin
        end
        
        // STORE
        5'b01000: begin
        end
        
        // OP
        5'b01100: begin
        end
        
        // LUI
        5'b01101: begin
        end
        
        // OP-32
        5'b01110: begin
        end
        
        // BRANCH
        5'b11000: begin
        end

        // JALR
        5'b11001: begin
        end

        // JAL
        5'b11011: begin
        end
        
        // Illegal or Unimplemented
        default: begin
            $display("Illegal Instruction: %x", instr);
        end


    endcase
end





endmodule
