`include "parameters.svh"
module ROBentry#(
    parameter PHYS_REG_COUNT = PHYS_REG_COUNT_H
    )(

    input clk,
    input rst_n,

    // bunch of srff inputs; maintain entry state
    // recommend using a task to allocate and retire entries
    input head_set,
    input head_reset,
    input tail_set,
    input tail_reset,
    input dispatched_set,
    input dispatched_reset,
    input executed_set,
    input executed_reset,
    input allocated_set,
    input allocated_reset,

    // register values set upon allocation
    input [$clog2(PHYS_REG_COUNT):0] dstreg_in,
    input [$clog2(PHYS_REG_COUNT):0] dstreg_old_in, // this preg is return upon instr complete
    input [$clog2(PHYS_REG_COUNT):0] src1reg_in,
    input [$clog2(PHYS_REG_COUNT):0] src2reg_in,

    // need control signals, probably output reg since there will be no logic

    // state signal output
    output head,
    output tail,
    output dispatched,
    output executed,
    output allocated,
    output reg [$clog2(PHYS_REG_COUNT):0] dstreg,
    output reg [$clog2(PHYS_REG_COUNT):0] dstreg_old,
    output reg [$clog2(PHYS_REG_COUNT):0] src1reg,
    output reg [$clog2(PHYS_REG_COUNT):0] src2reg
);

    // srffs to hold state values
    srff head(
        .clk(clk);
        .rst_n(rst_n);
        .set(head_set);
        .reset(head_reset);
        .q(head);
    );

    srff tail(
        .clk(clk);
        .rst_n(rst_n);
        .set(tail_set);
        .reset(tail_reset);
        .q(tail);
    );

    srff dispatched(
        .clk(clk);
        .rst_n(rst_n);
        .set(dispatched_set);
        .reset(dispatched_reset);
        .q(dispatched);
    );

    srff executed(
        .clk(clk);
        .rst_n(rst_n);
        .set(executed_set);
        .reset(executed_reset);
        .q(executed);
    );

    srff allocated(
        .clk(clk);
        .rst_n(rst_n);
        .set(allocated_set);
        .reset(allocated_reset);
        .q(allocated);
    );

// set and clear the values with allocated_set and allocated_reset
// probably don't need to clear if we're trying to save power
always @(posedge clk, negedge rst_n) begin
    if (~rst_n) begin
        dstreg <= 0;
        dstreg_old <= 0;
        src1reg <= 0;
        src2reg <= 0;
    end
    else if (allocated_set) begin
        dstreg <= dstreg_in;
        dstreg_old <= dstreg_old_in;
        src1reg <= src1reg_in;
        src2reg <= src2reg_in;
    end
    else if (allocated_reset) begin
        dstreg <= 0;
        dstreg_old <= 0;
        src1reg <= 0;
        src2reg <= 0;
    end
    else begin
        dstreg <= dstreg;
        dstreg_old <= dstreg_old;
        src1reg <= src1reg;
        src2reg <= src2reg;
    end

end

endmodule
